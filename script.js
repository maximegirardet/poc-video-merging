let constraints = {
    audio: false,
    video: true
}

function getScreen() {
    return navigator.mediaDevices.getDisplayMedia(constraints)
}

function getWebcam() {
    return navigator.mediaDevices.getUserMedia(constraints)
}

async function main() {

    try {
        let screenStream = await getScreen()
        let screenStream2 = await getScreen()
        let webcamStream = await getWebcam()

        console.log(screenStream)
        console.log(webcamStream)

        let merger = new VideoStreamMerger({width:1920, height: 1080})

        // Add the screen capture. Position it to fill the whole stream (the default)
        merger.addStream(screenStream, {
            x: 0, // position of the topleft corner
            y: 0,
            width: merger.width,
            height: merger.height,
            mute: true // we don't want sound from the screen (if there is any)
        })

        merger.addStream(screenStream2, {
            x: merger.width - 700, // position of the topleft corner
            y: merger.height - 500,
            width: 700,
            height: 500,
            mute: true // we don't want sound from the screen (if there is any)
        })

        // Add the webcam stream. Position it on the bottom left and resize it to 100x100.
        merger.addStream(webcamStream, {
            x: 0,
            y: merger.height - 300,
            width: 400,
            height: 300,
            mute: true
        })

        // Start the merging. Calling this makes the result available to us
        merger.start()
        window.res = merger

        // We now have a merged MediaStream!
        document.querySelector("#one").srcObject = merger.result
        document.querySelector("#launch_fps").addEventListener("click", () => {
            let value = parseInt(document.querySelector("#fps").value)
            screenStream.getVideoTracks()[0].applyConstraints({
                frameRate: value
            })
            alert(`Partage principal : ${value} FPS`)
        })
    } catch (e) {
        console.log(e)
        alert("Problème !")
    }

}

document.querySelector("#launch").addEventListener("click", main)
